package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import dto.Geometria;

class AppTest {


	public static Stream<Arguments> Sareacuadrado(){
		return Stream.of(
			Arguments.of(1, 1),
			Arguments.of(2, 4),
			Arguments.of(3, 9));
	}
	
	@ParameterizedTest
	@MethodSource("Sareacuadrado")
	public void testareacuadrado (int a, int b) {
		Geometria c = new Geometria();
		int result = c.areacuadrado(a);
		assertEquals(b, result);
	}
	
	
	public static Stream<Arguments> SareaCirculo(){
		return Stream.of(
			Arguments.of(2, 12.5664),
			Arguments.of(4, 50.2656),
			Arguments.of(11, 380.1336));
	}
	
	@ParameterizedTest
	@MethodSource("SareaCirculo")
	public void testareaCirculo (int a, double b) {
		Geometria c = new Geometria();
		double result = c.areaCirculo(a);
		assertEquals(b, result);
	}
	

	public static Stream<Arguments> Sareatriangulo(){
		return Stream.of(
			Arguments.of(1, 2, 1),
			Arguments.of(2, 3, 3),
			Arguments.of(12, 6, 36));
	}
	
	@ParameterizedTest
	@MethodSource("Sareatriangulo")
	public void testareatriangulo (int a, int b, int c) {
		Geometria d = new Geometria();
		int result = d.areatriangulo(a, b);
		assertEquals(c, result);
	}

	

	public static Stream<Arguments> Sarearectangulo(){
		return Stream.of(
			Arguments.of(9, 6, 54),
			Arguments.of(15, 3, 45),
			Arguments.of(20, 15, 300));
	}
	
	@ParameterizedTest
	@MethodSource("Sarearectangulo")
	public void testarearectangulo (int a, int b, int c) {
		Geometria d = new Geometria();
		int result = d.arearectangulo(a, b);
		assertEquals(c, result);
	}
	

	public static Stream<Arguments> Sareapentagono(){
		return Stream.of(
			Arguments.of(6, 2, 6),
			Arguments.of(8, 6, 24),
			Arguments.of(3, 8, 12));
	}
	
	@ParameterizedTest
	@MethodSource("Sareapentagono")
	public void testareapentagono (int a, int b, int c) {
		Geometria d = new Geometria();
		int result = d.areapentagono(a, b);
		assertEquals(c, result);
	}
	

	public static Stream<Arguments> Sarearombo(){
		return Stream.of(
			Arguments.of(2, 3, 3),
			Arguments.of(10, 4, 20),
			Arguments.of(16, 13, 104));
	}
	
	@ParameterizedTest
	@MethodSource("Sarearombo")
	public void testarearombo (int a, int b, int c) {
		Geometria d = new Geometria();
		int result = d.arearombo(a, b);
		assertEquals(c, result);
	}
	

	public static Stream<Arguments> Sarearomboide(){
		return Stream.of(
			Arguments.of(21, 3, 63),
			Arguments.of(12, 9, 108),
			Arguments.of(21, 4, 84));
	}
	
	@ParameterizedTest
	@MethodSource("Sarearomboide")
	public void testarearomboide (int a, int b, int c) {
		Geometria d = new Geometria();
		int result = d.arearomboide(a, b);
		assertEquals(c, result);
	}
	

	public static Stream<Arguments> Sareatrapecio(){
		return Stream.of(
			Arguments.of(1, 3, 12, 24),
			Arguments.of(2, 5, 6, 18),
			Arguments.of(12, 15, 16, 208));
	}
	
	@ParameterizedTest
	@MethodSource("Sareatrapecio")
	public void testareatrapecio (int a, int b, int c, int d) {
		Geometria e = new Geometria();
		int result = e.areatrapecio(a, b, c);
		assertEquals(d, result);
	}
	
	public static Stream<Arguments> Ssetter1(){
		return Stream.of(
				Arguments.of(1, 2.9, "cuadrado", "Geometria [id=" + 1 + ", nom=" + "cuadrado" + ", area=" + 2.9 + "]"),
		 		Arguments.of(2, 8.2, "Circulo", "Geometria [id=" + 2 + ", nom=" + "Circulo" + ", area=" + 8.2 + "]"),
				Arguments.of(3, 11.0, "Triangulo", "Geometria [id=" + 3 + ", nom=" + "Triangulo" + ", area=" + 11.0 + "]"),
				Arguments.of(4, 21.1, "Rectangulo", "Geometria [id=" + 4 + ", nom=" + "Rectangulo" + ", area=" + 21.1 + "]"),
				Arguments.of(5, 6.7, "Pentagono", "Geometria [id=" + 5 + ", nom=" + "Pentagono" + ", area=" + 6.7 + "]"),
				Arguments.of(6, 15.12, "Rombo", "Geometria [id=" + 6 + ", nom=" + "Rombo" + ", area=" + 15.12 + "]"),
				Arguments.of(7, 9.06, "Romboide", "Geometria [id=" + 7 + ", nom=" + "Romboide" + ", area=" + 9.06 + "]"),
				Arguments.of(8, 11.4, "Trapecio", "Geometria [id=" + 8 + ", nom=" + "Trapecio" + ", area=" + 11.4 + "]"));
	}
	
	@ParameterizedTest
	@MethodSource("Ssetter1")
	public void testsetter1(int a, double b, String c, String d) {
		Geometria e = new Geometria(a);
			e.setArea(b);
			int IDresult = e.getId();
			double AREAresult = e.getArea();
			String NOMresult = e.getNom(), TOSTRINGresult = e.toString();
			assertEquals(a, IDresult);
			assertEquals(b, AREAresult);
		   	assertEquals(c, NOMresult);
		   	assertEquals(d, TOSTRINGresult);
	}
	
	 private static Stream<Arguments> Ssetter2(){
		    return Stream.of(
		 		   Arguments.of(1, "cuadrado"),
		 		   Arguments.of(2, "Circulo"),
				   Arguments.of(3, "Triangulo"),
				   Arguments.of(4, "Rectangulo"),
				   Arguments.of(5, "Pentagono"),
				   Arguments.of(6, "Rombo"),
				   Arguments.of(7, "Romboide"),
				   Arguments.of(8, "Trapecio"));
	    }

		@ParameterizedTest
		@MethodSource("Ssetter2")
		public void testsetter2(int a, String b) {
			Geometria e = new Geometria();
			e.setId(a);
			e.setNom(b);
		   	int IDresult = e.getId();
		   	String STRINGresult = e.getNom(), FIGURAresult = e.figura(9);
		   	assertEquals(a, IDresult);
		   	assertEquals(b, STRINGresult);
		   	assertEquals("Default", FIGURAresult);
		}
	
}
